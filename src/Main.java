import core.Algorithm;
import core.Edge;
import core.Node;
import core.ReductionRule;

public class Main {
    public static void main(String[] args) {

        System.out.println("\nthis is the function that was presented in the research which is \n" +
                "f = (~x3 ^ x2 ^ x1) v (x3 ^ ~x2) v (x3 ^ ~x1) \n");


        Node zero = new Node(-2);
        Node one = new Node(-1);
        Node v = new Node(6,zero,one);
        Node u = new Node(5,one,one);
        Node t = new Node(4,one,zero);
        Node s = new Node(3,zero,zero);
        Node q = new Node(1,t,s);
        Node r = new Node(2,v,u);
        Node p = new Node(0,r,q);
        Edge edgePointingToRoot = new Edge(ReductionRule.S,false,p);
        Algorithm.setDepths(p);




        Edge reducedRoot =  Algorithm.reduceEdge(3,edgePointingToRoot);
        System.out.println("Results after Reduction");
        System.out.println("is root edge complemented: " + reducedRoot.getComplemented());
        System.out.println("root edge raw is: " + reducedRoot.getRaw());
        System.out.println("level of root edge: " + reducedRoot.getNode().getLevel());


        System.out.println("\nHigh edge");
        System.out.println(reducedRoot.getNode().getHigh().getRaw());
        System.out.println(reducedRoot.getNode().getHigh().getComplemented());
        System.out.println(reducedRoot.getNode().getHigh().getNode().getVal());

        System.out.println("\nlow edge");
        System.out.println(reducedRoot.getNode().getLow().getRaw());
        System.out.println(reducedRoot.getNode().getLow().getComplemented());
        System.out.println(reducedRoot.getNode().getLow().getNode().getVal());


        System.out.println("\nx1 = 1, x2 = 1, x3 = 1");
        System.out.println("Before Reduction : " + Algorithm.evaluateCESRBDD(3,edgePointingToRoot,"111"));
        System.out.println("After Reduction : " + Algorithm.evaluateCESRBDD(3,reducedRoot,"111"));


        System.out.println("\nx1 = 1, x2 = 0, x3 = 1");
        System.out.println("Before Reduction : " +Algorithm.evaluateCESRBDD(3,edgePointingToRoot,"101"));
        System.out.println("After Reduction : " + Algorithm.evaluateCESRBDD(3,reducedRoot,"101"));

        System.out.println("\nx1 = 0, x2 = 1, x3 = 0");
        System.out.println("Before Reduction : " +Algorithm.evaluateCESRBDD(3,edgePointingToRoot,"010"));
        System.out.println("After Reduction : " + Algorithm.evaluateCESRBDD(3,reducedRoot,"010"));


    }



}