package test;

import core.Algorithm;
import core.Edge;
import core.Node;
import core.ReductionRule;

public class test_1 {
    public static void main(String[] args){
        System.out.println("\nanother function to test  is  \n" +
                "f = (~x3 ^ ~x2 ^ ~x1) v (x1 ^ x2) v (x2 ^ x3) \n");


        Node zero = new Node(-2);
        Node one = new Node(-1);
        Node d = new Node(4,zero,one);
        Node e = new Node(5,one,zero);
        Node f = new Node(6,zero,zero);
        Node g = new Node(7,one,one);
        Node b = new Node(2,e,d);
        Node c = new Node(3,g,f);
        Node a = new Node(1,c,b);
        Edge edgePointingToRoot = new Edge(ReductionRule.S,false,a);
        Algorithm.setDepths(a);




        Edge reducedRoot =  Algorithm.reduceEdge(3,edgePointingToRoot);
        System.out.println("Results after Reduction");
        System.out.println("is root edge complemented: " + reducedRoot.getComplemented());
        System.out.println("root edge raw is: " + reducedRoot.getRaw());
        System.out.println("level of root edge: " + reducedRoot.getNode().getLevel());


        System.out.println("\nHigh edge");
        System.out.println(reducedRoot.getNode().getHigh().getRaw());
        System.out.println(reducedRoot.getNode().getHigh().getComplemented());
        System.out.println(reducedRoot.getNode().getHigh().getNode().getVal());

        System.out.println("\nlow edge");
        System.out.println(reducedRoot.getNode().getLow().getRaw());
        System.out.println(reducedRoot.getNode().getLow().getComplemented());
        System.out.println(reducedRoot.getNode().getLow().getNode().getVal());


        System.out.println("\nx1 = 1, x2 = 1, x3 = 1");
        System.out.println("Before Reduction : " + Algorithm.evaluateCESRBDD(3,edgePointingToRoot,"111"));
        System.out.println("After Reduction : " + Algorithm.evaluateCESRBDD(3,reducedRoot,"111"));


        System.out.println("\nx1 = 1, x2 = 0, x3 = 1");
        System.out.println("Before Reduction : " +Algorithm.evaluateCESRBDD(3,edgePointingToRoot,"101"));
        System.out.println("After Reduction : " + Algorithm.evaluateCESRBDD(3,reducedRoot,"101"));

        System.out.println("\nx1 = 0, x2 = 1, x3 = 0");
        System.out.println("Before Reduction : " +Algorithm.evaluateCESRBDD(3,edgePointingToRoot,"010"));
        System.out.println("After Reduction : " + Algorithm.evaluateCESRBDD(3,reducedRoot,"010"));


        System.out.println("\nx1 = 0, x2 = 0, x3 = 0");
        System.out.println("Before Reduction : " +Algorithm.evaluateCESRBDD(3,edgePointingToRoot,"000"));
        System.out.println("After Reduction : " + Algorithm.evaluateCESRBDD(3,reducedRoot,"000"));

        System.out.println("\nx1 = 0, x2 = 1, x3 = 1");
        System.out.println("Before Reduction : " +Algorithm.evaluateCESRBDD(3,edgePointingToRoot,"011"));
        System.out.println("After Reduction : " + Algorithm.evaluateCESRBDD(3,reducedRoot,"011"));


    }

}
