package core;

public class Node {
    private int val;
    private Edge high;
    private Edge low;

    private static int id = 10000;

    static Node zeroNode = new Node(-2,null,null);

    public Node(){
        this.val = id--;
    }

    public Node(int value){
        this.val = value;

    }
    public Node(int value,Node high , Node low )
    {
        this.val = value;
        this.high = new Edge(ReductionRule.S,false,high);
        this.low = new Edge(ReductionRule.S,false,low);
    }

    private int level;

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public Edge getHigh() {
        return high;
    }

    public Edge getLow() {
        return low;
    }

    public int getVal() {
        return val;
    }

    public void setHigh(Edge high) {
        this.high = high;
    }

    public void setLow(Edge low) {
        this.low = low;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public boolean isRedundant(){
        if(this.getHigh() == null || this.getLow() == null)throw new RuntimeException("it should not be called on level zero nodes");
        if(this.getHigh().getNode() == this.getLow().getNode() && this.getHigh().getComplemented() == this.getLow().getComplemented()) {
            return (this.getHigh().getRaw() == ReductionRule.X || this.getHigh().getRaw() == ReductionRule.S) &&
                    (this.getLow().getRaw() == ReductionRule.X || this.getLow().getRaw() == ReductionRule.S);
        }
        return false;
    }

    public boolean isHighZero(){
        if(this.getHigh().getNode().getVal() == -2 && !this.getHigh().getComplemented() &&
                (this.getHigh().getRaw() == ReductionRule.S || this.getHigh().getRaw() == ReductionRule.X)){
            return this.getLow().getRaw() == ReductionRule.H0 || this.getLow().getRaw() == ReductionRule.S;
        }
        return false;
    }

    public boolean isHighOne(){
        if(this.getHigh().getNode().getVal() == -2 && this.getHigh().getComplemented() &&
                (this.getHigh().getRaw() == ReductionRule.S || this.getHigh().getRaw() == ReductionRule.X)){
            return this.getLow().getRaw() == ReductionRule.H1 || this.getLow().getRaw() == ReductionRule.S;
        }
        return false;
    }

    public boolean isLowZero() {
        if(this.getLow().getNode().getVal() == -2 && !this.getLow().getComplemented() &&
                ((this.getHigh().getRaw() == ReductionRule.S || this.getHigh().getRaw() == ReductionRule.X))){
            return this.getLow().getRaw() == ReductionRule.L0 || this.getLow().getRaw() == ReductionRule.S;
        }
        return false;
    }


    public boolean isX1andX2Node() {
        return (this.getLow().getNode().getVal() == -2 && this.getHigh().getNode().getVal() == -2)
                &&
                ((this.getLow().getRaw() == ReductionRule.X && this.getHigh().getRaw() == ReductionRule.H1
                        && !this.getHigh().getComplemented() && !this.getLow().getComplemented())
                        ||
                        (this.getLow().getRaw() == ReductionRule.X && this.getLow().getComplemented()
                                && this.getHigh().getComplemented() && this.getHigh().getRaw() == ReductionRule.H0));
    }
}

