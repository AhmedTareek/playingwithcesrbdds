package core;

public enum ReductionRule{
    X, //skipped values are irrelevant along the path
    H0, //function has value 0 if any skipped var has value 1
    L0, //function has value 0 if any skipped var has value 0
    H1, //function has value 1 if any skipped var has value 1
    L1, //function has value 1 if any skipped var has value 0
    S //it means short edge (a normal edge with no reduction rules)
}
