package core;

public class Algorithm {

    public static void setDepths(Node root) {
        int maxDepth = getMaxDepth(root);
        setDepthsHelper(root,maxDepth);
    }
    private static void setDepthsHelper(Node root,int maxDepth){
        root.setLevel(maxDepth);
        if(root.getHigh()!=null) {
            setDepthsHelper(root.getHigh().getNode(), maxDepth-1);
        }
        if(root.getLow()!=null) {
            setDepthsHelper(root.getLow().getNode(), maxDepth -1);
        }
    }
    public static int getMaxDepth(Node root) {
        if(root.getHigh() != null && root.getLow() != null) {
            return Math.max(getMaxDepth(root.getLow().getNode()), getMaxDepth(root.getHigh().getNode())) + 1;
        }else if (root.getLow() != null) {
            return getMaxDepth(root.getLow().getNode()) + 1;
        } else if (root.getHigh()!=null) {
            return getMaxDepth(root.getHigh().getNode()) + 1;
        }else {
            return 0;
        }
    }


    public static Edge reduceEdge(int level,Edge edge) {
        Edge e = reduceNode(edge.getNode());
        if (edge.getComplemented()) {
            //e.setComplemented(!e.getComplemented());
            e.complement();
        }
        return mergeEdge(level,edge.getRaw(),e,edge.getNode().getLevel());
    }

    private static Edge reduceNode(Node node){


       Edge e;
       if(node.getLevel() == 0) return new Edge(ReductionRule.S, node.getVal() == -1,Node.zeroNode);
       Node newNode = new Node();
       newNode.setLevel(node.getLevel());
       newNode.setLow(reduceEdge(node.getLevel()-1, node.getLow()));
       newNode.setHigh(reduceEdge(node.getLevel()-1, node.getHigh()));
       boolean c = newNode.getLow().getComplemented();
       if(c)
       {
            newNode.getHigh().complement();
            newNode.getLow().complement();
       }
       if(newNode.isRedundant()){
           e = newNode.getLow();
           e.setRaw(ReductionRule.X);
       }else if (newNode.isHighZero()){
           e = newNode.getLow();
           e.setRaw(ReductionRule.H0);
       }else if (newNode.isHighOne()){
           e = newNode.getLow();
           e.setRaw(ReductionRule.H1);
       }else if (newNode.isLowZero()){
           e = newNode.getHigh();
           e.setRaw(ReductionRule.L0);
       }else if (newNode.isX1andX2Node()){
           e = new Edge(ReductionRule.L0,true,Node.zeroNode);
       }else{
           e = new Edge(ReductionRule.S,false,newNode);
       }
       if(c){
           e.complement();
       }
       return e;

    }

    private static Edge mergeEdge(int n, ReductionRule rule, Edge e, int lvlP)
    {
        if(e.getRaw() == ReductionRule.S) {
            e.setRaw(rule);
        }
        if(lvlP == 1 && e.getRaw() == ReductionRule.H0 && e.getComplemented()
                && e.getNode().getVal() == -2 && rule == ReductionRule.L1) {
            return new Edge(ReductionRule.L1,false,Node.zeroNode);
        }
        if(lvlP == 1 && e.getRaw() == ReductionRule.H1 && !e.getComplemented() && e.getNode().getVal() == -2
                && rule == ReductionRule.L0){
            return new Edge(ReductionRule.L0,true,Node.zeroNode);
        }

        if(e.getRaw() == ReductionRule.X && !e.getComplemented() && e.getNode().getVal() == -2
                && (e.getRaw() == ReductionRule.X || e.getRaw() == ReductionRule.S ||
                e.getRaw() == ReductionRule.L0 || e.getRaw() == ReductionRule.H0)
        ){
            return new Edge(ReductionRule.X,false,Node.zeroNode);
        }
        if(n == 1 && e.getRaw() == ReductionRule.L1 && !e.getComplemented() && e.getNode().getVal() == -2) {
            return new Edge(ReductionRule.H0,true,Node.zeroNode);
        }else if (n==1 && e.getRaw() == ReductionRule.L0 && e.getComplemented() && e.getNode().getVal() == -2){
            return new Edge(ReductionRule.H1,false,Node.zeroNode);
        }
        if(rule == e.getRaw() || rule ==ReductionRule.S)return e;
        Node q = new Node();
        q.setLevel(lvlP + 1);
        if(rule == ReductionRule.X){
            q.setLow(e);
            q.setHigh(e);
        } else if (rule == ReductionRule.L1 || rule == ReductionRule.L0) {
            q.setLow(new Edge(ReductionRule.X,rule==ReductionRule.L1,Node.zeroNode));
            q.setHigh(e);
        } else if (rule == ReductionRule.H1 || rule == ReductionRule.H0) {
            q.setLow(e);
            q.setHigh(new Edge(ReductionRule.X,rule==ReductionRule.H1,Node.zeroNode));
        }
        e.setComplemented(q.getLow().getComplemented());
        if(e.getComplemented()){
            q.getLow().complement();
            q.getHigh().complement();
        }
        if(n==q.getLevel()){
            e.setRaw(ReductionRule.S);
        }else{
            e.setRaw(rule);
        }

        return e;
    }

    public static boolean evaluateCESRBDD(int n, Edge e, String varValues) {
        if(e.getNode().getLevel() == n && n == 0){
            return e.getComplemented() ^ (e.getNode().getVal() == -1);
        }
        if(e.getNode().getLevel() == n && n > 0){
            if(varValues.endsWith("1")){
                return e.getComplemented() ^ evaluateCESRBDD(n-1,e.getNode().getHigh(), varValues.substring(0,varValues.length() - 1));
            }else{
                return e.getComplemented() ^ evaluateCESRBDD(n-1, e.getNode().getLow(), varValues.substring(0,varValues.length() - 1));
            }
        }
        if(e.getNode().getLevel() < n && e.getRaw() == ReductionRule.X){
            return evaluateCESRBDD(n-1, e,varValues.substring(0,varValues.length() - 1));
        }
        if(e.getNode().getVal() < n && (e.getRaw() == ReductionRule.L1 || e.getRaw() == ReductionRule.L0)){
            if(varValues.endsWith("1")){
                return evaluateCESRBDD(n-1, e, varValues.substring(0,varValues.length() - 1));
            }else{
                return e.getRaw() == ReductionRule.L1;
            }
        }
        if(e.getNode().getVal() < n && (e.getRaw() == ReductionRule.H1 || e.getRaw() == ReductionRule.H0)){
            if(varValues.endsWith("1")){
                return e.getRaw() == ReductionRule.H1;
            }else{
                return evaluateCESRBDD(n-1, e,varValues.substring(0,varValues.length() - 1));
            }
        }
        throw new RuntimeException("cant reach here");

    }
}
