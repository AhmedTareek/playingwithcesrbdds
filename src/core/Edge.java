package core;

public class Edge{

    public Edge(){}

    public Edge(ReductionRule rule,boolean complemented,Node node){
        this.complemented = complemented;
        this.node = node;
        this.raw = rule;
    }
    private ReductionRule raw;
    private boolean complemented;
    private Node node;

    public void setComplemented(boolean complemented) {
        this.complemented = complemented;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public void setRaw(ReductionRule raw) {
        this.raw = raw;
    }

    public boolean getComplemented() {
        return complemented;
    }

    public Node getNode() {
        return node;
    }

    public ReductionRule getRaw() {
        return raw;
    }

    public void complement(){
        this.setComplemented(!this.getComplemented());

        if (this.raw == ReductionRule.L0){
            this.raw = ReductionRule.L1;
        }else if (this.raw == ReductionRule.H0){
            this.raw = ReductionRule.H1;
        }else if (this.raw == ReductionRule.L1){
            this.raw = ReductionRule.L0;
        } else if (this.raw == ReductionRule.H1) {
            this.raw = ReductionRule.H0;
        }

    }
}
